import React, { useState } from 'react'
import { NavLink } from 'react-router-dom'
import profilePicLink from '../img/profilePicLink.png'
import { useTranslation } from 'react-i18next'

export default function Main(props) {

    const[open, setOpen] = useState(false);
    const {t, i18n } = useTranslation();

    const DropDownLanguage = () => (

        i18n.language==="en" ? 

        <div className="dropMenuLangMain">
            <button className="dropMenuItem" onClick={() => i18n.changeLanguage("ua")}>UA</button>
            <button className="dropMenuItem"onClick={() => i18n.changeLanguage("pl")}>PL</button>
            <button className="dropMenuItem"onClick={() => i18n.changeLanguage("ru")}>RU</button>
        </div> : 

        i18n.language==="ua" ? 
        <div className="dropMenuLangMain">
            <button className="dropMenuItem" onClick={() => i18n.changeLanguage("en")}>EN</button>
            <button className="dropMenuItem"onClick={() => i18n.changeLanguage("pl")}>PL</button>
            <button className="dropMenuItem"onClick={() => i18n.changeLanguage("ru")}>RU</button>
        </div> : 

        i18n.language==="pl" ? 
        <div className="dropMenuLangMain">
            <button className="dropMenuItem"onClick={() => i18n.changeLanguage("ua")}>UA</button>
            <button className="dropMenuItem" onClick={() => i18n.changeLanguage("en")}>EN</button>
            <button className="dropMenuItem"onClick={() => i18n.changeLanguage("ru")}>RU</button>
        </div> : 
        
        <div className="dropMenuLangMain">
            <button className="dropMenuItem"onClick={() => i18n.changeLanguage("ua")}>UA</button>
            <button className="dropMenuItem"onClick={() => i18n.changeLanguage("pl")}>PL</button>
            <button className="dropMenuItem" onClick={() => i18n.changeLanguage("en")}>EN</button>
        </div> 
      )

    return(
        <>
            <header className="headerMain">
                <p className="logo">Linkstagram</p>



                <div className="navMain">
                    <NavLink to="/" className="homeLink">
                        <p className="homeText">{t('homeBtn')}</p>
                    </NavLink>

                    <button className="btnLang" onClick={() => setOpen(!open)}>
                        <p className="lang_id">{ i18n.language }</p>
                    </button>

                    <NavLink to="/profile" className="userLink">
                        <img className="profilePicLink" src={profilePicLink} />
                    </NavLink>
                </div>


            </header>

            { open ? <DropDownLanguage /> : null }
        </>
    )
}
