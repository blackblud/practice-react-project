import React from 'react'
import MainHeader from './MainHeader'
import { useTranslation } from 'react-i18next'
import Translation from './Translation'

export default function None404() {

    const {t, i18n} = useTranslation();

    return (
        <>
            <MainHeader />
                <div id="notfound">
                    <div class="notfound">
                        <div class="notfound-404">
                            <h3>{t("Page404Header")}</h3>
                            <h1><span>4</span><span>0</span><span>4</span></h1>
                        </div>
                        <h2>{t("Page404Text")}</h2>
                    </div>
                </div>
        </>
    )
}
