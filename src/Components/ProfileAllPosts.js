import React, { Component } from 'react'
import ProfilePost from './ProfilePost'

export default class ProfileAllPosts extends Component {

    constructor(props) {
        super();
        this.state = {
            error: null,
            isLoaded: false,
            postsData: []
        };
    }

    componentDidMount(){
        
        fetch("https://linkstagram-api.ga/profiles/"+ this.props.userData.username + "/posts", {
            method: 'GET',
            headers: { 
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': localStorage.getItem("Authorization")
            }})
            .then(response => response.json())
            .then(
                (response) => {
                    this.setState({
                        isLoaded: true,
                        postsData: response
                    });
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    })
                }
                );
                
    }

    render() {
        const {error, isLoaded, postsData} = this.state;
        if(error){
            return <p>Error {error.message}</p>
        } else if (!isLoaded){
            return <p>All User's Posts Loading...</p>
        } else {
        return(
            <div className="posts">
                {postsData.map(postData => (
                    <ProfilePost postDataInfo={postData}/>
                    )   
                )}
            </div>
        )
    }
    }
}

