import React, { createRef, useState } from 'react';
import { useHistory } from 'react-router-dom'
import uploadPhotoPlaceholder from '../img/uploadPhotoPlaceholder.png'
import Translation from './Translation'

const ModalNew = ({ handleClose, show }) => {
  const showHideClassName = show ? "modal display-block" : "modal display-none";

    let description = createRef();

    function createNewPost(e){
      e.preventDefault();

      console.log('Post Created - ' + description.current.value);
    }

  return (
    <div className={showHideClassName}>
      <section className="modal-main">

        <form onSubmit={createNewPost}>

          <div className="editBody">
            <div className="uploadPhotoDiv">
              <img className="uploadPhotoPlaceholder" src={uploadPhotoPlaceholder}/>
            </div>

            <div className="editBodyDownNew">
               <div className="inputDivEdit"> 
                  <p className="textareaNameEdit"><Translation value="desc"/></p>
                  <textarea ref={description} className="textareaTypeEdit" type="textarea" required="required"/>
              </div>
            </div>
          </div>



        <div className="editFooter"> 
          <button className="modalBtnCancel" type="button" onClick={handleClose}><Translation value="cancel"/></button>
          <button type="submit" className="modalBtnSave"><Translation value="save"/></button>
        </div>
      
        </form>
      </section>
    </div>
  );
};

export default ModalNew