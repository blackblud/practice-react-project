import React from 'react'
import profilePostImage from '../img/profilePostImage.png'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTrashAlt } from '@fortawesome/free-solid-svg-icons'
import { useTranslation } from 'react-i18next'
import Translation from './Translation'


export default function ProfilePost(props) {

    const {t, i18n} = useTranslation();

    function deletePost(){
        
        let choice = window.confirm(t("deletePostAlert") + props.postDataInfo.id + " ?");
    
        if (!choice) {
            console.log("NO!");
        } else {
            console.log("YES!");
            const dataRequest = {
                method: 'DELETE',
                headers: { 
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': localStorage.getItem("Authorization")
                }
              };
          
              fetch("https://linkstagram-api.ga/posts/" + props.postDataInfo.id, dataRequest)
              .then(response => {
                  if(response.ok){
                    console.log("Final - all good!");
                    window.location.reload();
                  } else {
                    console.log("Smth bad...");
                  }
              })
        }
    
    }

    return (
        <div className="profilePost">
            <img className="profilePostImg" src={props.postDataInfo.photos.[0].url}/>

            <div className="getImageID" onClick={deletePost}><Translation value="deletePost"/><FontAwesomeIcon icon={faTrashAlt} className="iconDelete"/></div>
        </div>
    )
}
