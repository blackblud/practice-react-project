import React from 'react'
import testPhoto from '../img/story/storyPic4.png'

export default function SingleComment(props) {

    const monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

    return(
        <div className="singleComment">
            <img className="singleCommentImg" src={props.data.commenter.profile_photo_url}/>
            
            <div className="singleCommentData">
                <p className="singleCommentMSG">{props.data.message}</p>
                <p className="singleCommentTime">{new Date(props.data.created_at).getDate() + " " + monthNames[new Date(props.data.created_at).getMonth()] + " " + new Date(props.data.created_at).getUTCHours() + ":" + new Date(props.data.created_at).getUTCMinutes() + " UTC"}</p>
            </div>
        </div>
    )
}
