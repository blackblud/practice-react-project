import React, { useState } from 'react'
import postUserImage from '../img/profilePicLink.png'
import postImageImg from '../img/PostImage.png'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faComment, faHeart, faEllipsisV, faArrowRight, faLink } from '@fortawesome/free-solid-svg-icons'
import { useTranslation } from 'react-i18next'
import ModalComments from './ModalComments'



import storyPic1 from '../img/story/storyPic1.png'
import storyPic2 from '../img/story/storyPic2.png'
import storyPic3 from '../img/story/storyPic3.png'
import storyPic4 from '../img/story/storyPic4.png'
import storyPic5 from '../img/story/storyPic5.png'
import storyPic6 from '../img/story/storyPic6.png'
import storyPic7 from '../img/story/storyPic7.png'
import storyPic8 from '../img/story/storyPic8.png'
import storyPic9 from '../img/story/storyPic9.png'

export default function Post(props) {

    //console.log('ID [' + props.data.id + "] - " + props.data.is_liked)

    const[liked, setLike] = useState(props.data.is_liked); 
    const[countLike, setCountLike] = useState(props.data.likes_count)
    const[copied, setCopied] = useState();
    const[showComment, setShowComment] = useState(false);
    const[updatee, setUpdate] = useState(false)

    const {t, i18n} = useTranslation();

    let reservePictures = [storyPic1, storyPic2, storyPic3, storyPic4, storyPic5, storyPic6, storyPic7, storyPic8, storyPic9];

    const monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

    function sharePost(){
        navigator.clipboard.writeText("https://linkstagram-api.ga/posts/" + props.data.id);
        setCopied("copied-x");
        setTimeout(function(){ setCopied("") }, 1500);
        
    }

    function like(){
        if(liked) {
            console.log("Like UNSET");

            fetch("https://linkstagram-api.ga/posts/" + props.data.id + "/like", {
                method: 'DELETE',
                headers: { 
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': localStorage.getItem("Authorization")
                }});

            setCountLike(countLike - 1);

            setLike(!liked);
        } else {
            console.log("Like SET");

            fetch("https://linkstagram-api.ga/posts/" + props.data.id + "/like", {
                method: 'POST',
                headers: { 
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': localStorage.getItem("Authorization")
                }})


            setCountLike(countLike + 1);

            setLike(!liked);
        }
    }   

    function showCommentModule(){
        console.log("Opened Module");
        setShowComment(true);

    }

    function hideCommentModule(){
        console.log("Hiden Module");
        setShowComment(false);

    }

    return(
        <>
        <ModalComments updateVar={updatee} cabak={() => setUpdate()} data={props.data} show={showComment} handleClose={hideCommentModule}/>

        <div className="post">
            <div className="postHeader">
                <div className="postAuthorInfo">
                    <img className="postUserImage" src={props.data.author.profile_photo_url==null ? reservePictures[Math.floor(Math.random() * reservePictures.length)] : props.data.author.profile_photo_url} />
                    
                    <div className="postUserInfo">
                        <p>{props.data.author.first_name==null ? "Empty Name" : props.data.author.first_name} {props.data.author.last_name==null ? "" : props.data.author.last_name}</p>
                        <p>{
                        
                        new Date(props.data.created_at).getDate() + " " + monthNames[new Date(props.data.created_at).getMonth()]  + " " + new Date(props.data.created_at).getUTCHours() + ":" + new Date(props.data.created_at).getUTCMinutes()
                    
                    
                    }</p>
                    </div>
                </div>
                <div className="postGlobalControls">
                    <FontAwesomeIcon icon={faEllipsisV} className="iconDots"/>
                </div>
            </div>


            <div className="postMain">
                <div className="postImage">
                    <img className="postImageImg" src={props.data.photos.length === 0 ? null :props.data.photos.[0].url} />

                    <div className={"copied " + copied}><FontAwesomeIcon icon={faLink} className="iconChain"/> {t("copied")}</div>
                </div>
                <div className="postDescription">
                    <p>{props.data.description}</p>
                </div>
                
            </div>



            <div className="postFooter">
                <div className="postControls">

                    { liked 
                    ? <FontAwesomeIcon onClick={like} icon={faHeart} className="iconLiked"></FontAwesomeIcon>
                    : <FontAwesomeIcon onClick={like} icon={faHeart} className="iconLike"></FontAwesomeIcon> }

                    <p className="countLikes">{countLike}</p>

                    <FontAwesomeIcon onClick={showCommentModule} icon={faComment} className="iconComment"/> 
                </div>
                <div className="postShare" onClick={sharePost}>
                {t('share')} <FontAwesomeIcon icon={faArrowRight} className="iconShare"/>
                </div>
            </div>
        </div>
        </>
    )
}
