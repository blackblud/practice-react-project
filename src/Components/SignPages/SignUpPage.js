import React, { createRef } from 'react'
import SignUpPicture from './SignUpPicture'
import SignHeader from './SignHeader'
import { NavLink } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import { useHistory } from 'react-router-dom'

export default function SignInPage(props, context) {

    const {t, i18n} = useTranslation();
    function changeLanguage(lang) {
        i18n.changeLanguage(lang);
    }

    const history = useHistory();

    

    let emailInput = createRef();
    let usernameInput = createRef();
    let passInput = createRef();

    function userSignUp(e) {

        e.preventDefault();

        console.log("Email Input - " + emailInput.current.value);
        console.log("UserName Input - " + usernameInput.current.value);
        console.log("Password Input - " + passInput.current.value);
 
        const dataRequest = {
            method: 'POST',
            headers: { 
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                "username": usernameInput.current.value,
                "login": emailInput.current.value,
                "password": passInput.current.value
            })
        };

        fetch('https://linkstagram-api.ga/create-account', dataRequest)
        .then(response => {
            if(response.ok){
                history.push("/signin");
            } else {
                response.json().then(response => alert("Error [" + response.['field-error'].[0]+"]\n"+
                                                        "Message - " + response.['field-error'].[1]));
            }
        })
    }

    return(
        <>
            <SignHeader funct={changeLanguage}/>
            <div className="wrapperSign">

                <div className="signBlock">
                    <SignUpPicture />
                    <form className="signForm" onSubmit={userSignUp}>
                        <div className="signUpH1">
                            <h1>{t('signUp')}</h1>
                        </div>
                        {/* <Input ref={emailInput} selector="email" name={t('email')} placeholder={t('emailPlaceholder')}/> */
                        /* <Input name={t('username')} placeholder={t('usernamePlaceholder')}/>
                        <Input name={t('pass')} placeholder={t('passPlaceholder')}/> */}




                        {/* безполезний клас InputDiv, але без нього <p> не піддається центруванню через align-items:center */}
                        <div className="inputDiv" selector="email"> 
                            <p className="inputName">{t('email')}</p>
                            <input ref={emailInput} className="inputType" placeholder={t('emailPlaceholder')} type="email" required="required"/>
                        </div>

                        <div className="inputDiv"> 
                            <p className="inputName">{t('username')}</p>
                            <input ref={usernameInput} className="inputType" placeholder={t('usernamePlaceholder')} type="text" required="required"/>
                        </div>

                        <div className="inputDiv"> 
                            <p className="inputName">{t('pass')}</p>
                            <input ref={passInput} className="inputType" placeholder={t('passPlaceholder')} type="password" required="required"/>
                        </div>









                        {/* цей div, що внизу був компонентом button, але він перезагружав сторінку і я не знав як взяти дані з інпутів в нього */}
                        <button type="submit" className="signButton">
                            <p>{t('signUp')}</p>
                        </button>
                        <p className="p_up">{t('haveaccount')}<NavLink to="/signin">{t('signIn')}</NavLink> </p>
                    </form>
                </div>
            </div>
        </>
    )
}