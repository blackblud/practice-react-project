import React, { createRef } from 'react'
import SignInPicture from './SignInPicture'
import SignHeader from './SignHeader'
import { NavLink } from 'react-router-dom'
import { useTranslation } from 'react-i18next'

import { useHistory } from 'react-router-dom'

export default function SignInPage() {

    const {t, i18n} = useTranslation();

    function changeLanguage(lang) {
        i18n.changeLanguage(lang);
    }

    const history = useHistory();

    

    let emailInput = createRef();
    let passInput = createRef();

    function userSignUp(e) {


        e.preventDefault();

        console.log("Email Input - " + emailInput.current.value);
        console.log("Password Input - " + passInput.current.value);
 
        const dataRequest = {
            method: 'POST',
            headers: { 
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                "login": emailInput.current.value,
                "password": passInput.current.value
            })
        };

        fetch('https://linkstagram-api.ga/login', dataRequest)
        .then(response => {
            if(response.ok){
                localStorage.setItem("Authorization", response.headers.get('authorization'));
                history.push("/");
            } else {
                response.json().then(response => alert("Error [" + response.['field-error'].[0]+"]\n"+
                                                        "Message - " + response.['field-error'].[1]));
            }
        })
    }

    return(
        <>
            <SignHeader funct={changeLanguage}/> 
            <div className="wrapperSign">

                <div className="signBlock">

                    <SignInPicture />

                    <form className="signForm" autoComplete="true" onSubmit={userSignUp}>
                        <div className="signUpH1">
                            <h1>{t('signIn')}</h1>
                        </div>
                        <div className="inputDiv" selector="email"> 
                            <p className="inputName">{t('email')}</p>
                            <input ref={emailInput} className="inputType" placeholder={t('emailPlaceholder')} type="email" required="required"/>
                        </div>

                        
                        <div className="inputDiv"> 
                            <p className="inputName">{t('pass')}</p>
                            <input ref={passInput} className="inputType" placeholder={t('passPlaceholder')} type="password" required="required"/>
                        </div>

                        <button type="submit" className="signButton">
                            <p>{t('signIn')}</p>
                        </button>
                        <p className="p_in">{t('donthaveaccount')}<NavLink to="/signup">{t('signUp')}</NavLink> </p>
                    </form>
                </div>
            </div>
        </>
    )
}