import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'

export default function SignHeader(props) {

    const[open, setOpen] = useState(false);
    const { i18n } = useTranslation();

    const DropDownLanguage = () => (

        i18n.language==="en" ? 

        <div className="dropMenuLang">
            <button className="dropMenuItem" onClick={() => props.funct("ua")}>UA</button>
            <button className="dropMenuItem"onClick={() => props.funct("pl")}>PL</button>
            <button className="dropMenuItem"onClick={() => props.funct("ru")}>RU</button>
        </div> : 

        i18n.language==="ua" ? 
        <div className="dropMenuLang">
            <button className="dropMenuItem" onClick={() => props.funct("en")}>EN</button>
            <button className="dropMenuItem"onClick={() => props.funct("pl")}>PL</button>
            <button className="dropMenuItem"onClick={() => props.funct("ru")}>RU</button>
        </div> : 

        i18n.language==="pl" ? 
        <div className="dropMenuLang">
            <button className="dropMenuItem"onClick={() => props.funct("ua")}>UA</button>
            <button className="dropMenuItem" onClick={() => props.funct("en")}>EN</button>
            <button className="dropMenuItem"onClick={() => props.funct("ru")}>RU</button>
        </div> : 
        
        <div className="dropMenuLang">
        <button className="dropMenuItem"onClick={() => props.funct("ua")}>UA</button>
        <button className="dropMenuItem"onClick={() => props.funct("pl")}>PL</button>
        <button className="dropMenuItem" onClick={() => props.funct("en")}>EN</button>
    </div> 
      )

    return(
        <>
            <header>
                <p className="logo">Linkstagram</p>
                <button className="btnLang" onClick={() => setOpen(!open)}>
                    <p className="lang_id">{ i18n.language }</p>
                </button>
            </header>

            { open ? <DropDownLanguage function={props.funct}/> : null }
        </>
    )
}
