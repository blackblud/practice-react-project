import React, { useState }from 'react'
import picMain from '../../img/signInPicNew.png'
import picture1 from '../../img/pic1.png'
import picture2 from '../../img/pic2.png'
import picture3 from '../../img/pic3.png'


export default function SignInPicture() {
    return(
        <div className="signInPicture">

            <img className="signInPicMain" src={picMain} />

            {/* <CSSTransition
                in={true}
                timeout={500}
                className={'signInPic1'}
                unmountOnExit>

                <img src={picture1} />

            </CSSTransition> */}

            {/* Не працює добавляння -класів (entered, entering, exited) до картинки*/}
            
            <img className="signInPic1" src={picture1} />
            <img className="signInPic2" src={picture2} />
            <img className="signInPic3" src={picture3} />
        </div>
    )
}
