import React from 'react'
import picMain from '../../img/signInPicNew.png'
import picture1 from '../../img/pic1.png'
import picture2 from '../../img/pic2.png'
import picture3 from '../../img/pic3.png'

export default function SignInPicture() {
    return(
        <div className="signInPicture">

            <img className="signUpPicMain" src={picMain} />
            
            <img className="signUpPic1" src={picture1} />
            <img className="signUpPic2" src={picture2} />
            <img className="signUpPic3" src={picture3} />

        </div>
    )
}
