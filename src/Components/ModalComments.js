import React, { createRef, useState } from 'react';
import { useHistory } from 'react-router-dom'
import uploadPhotoPlaceholder from '../img/uploadPhotoPlaceholder.png'
import Translation from './Translation'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTimes, faHeart } from '@fortawesome/free-solid-svg-icons'
import Comments from './Comments'

const ModalComments = ({ handleClose, show, data, cabak, updatee}) => {
  const showHideClassName = show ? "modal display-block" : "modal display-none";

  const[liked, setLike] = useState(data.is_liked); 
  const[countLike, setCountLike] = useState(data.likes_count)
  const[newComment, setNewComment] = useState(false)

  let commentValue = createRef();

  function like(){
    if(liked) {
      console.log("Like UNSET");

      fetch("https://linkstagram-api.ga/posts/" + data.id + "/like", {
          method: 'DELETE',
          headers: { 
              'Accept': 'application/json',
              'Content-Type': 'application/json',
              'Authorization': localStorage.getItem("Authorization")
          }});

      setCountLike(countLike - 1);

      setLike(!liked);
  } else {
      console.log("Like SET");

      fetch("https://linkstagram-api.ga/posts/" + data.id + "/like", {
          method: 'POST',
          headers: { 
              'Accept': 'application/json',
              'Content-Type': 'application/json',
              'Authorization': localStorage.getItem("Authorization")
          }})


      setCountLike(countLike + 1);

      setLike(!liked);
  }
}

  function postComment(e){
    e.preventDefault();

    console.log("Comment - " + commentValue.current.value);




    const dataRequest = {
      method: 'POST',
      headers: { 
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': localStorage.getItem("Authorization")
      },
      body: JSON.stringify({
          "message": commentValue.current.value
      })
  };

  fetch("https://linkstagram-api.ga/posts/"+ data.id + "/comments", dataRequest)
  .then(response => {
      if(response.ok){
        console.log("ALL GOOD");
        commentValue.current.value = ""
        // cabak(true); 
      } else {
          response.json().then(response => alert("Error [" + response.['field-error'].[0]+"]\n"+
                                                  "Message - " + response.['field-error'].[1]));
      }
  })



  }

  return (
    <div className={showHideClassName}>
      <section className="modalMainComments">




        <div className="partPhoto">
          <img className="partPhotoImg" src={data.photos.length === 0 ? null : data.photos.[0].url}/>
        </div>
        <div className="partComments">
          <div className="commentsHeader">
            <div className="comHeaderUserInfo">
            <img className="comHeaderImageUser" src={data.author.profile_photo_url}/>

            <p>{data.author.first_name + " " + data.author.last_name}</p>
            </div>
            {console.log(data.author.profile_photo_url)}

            <FontAwesomeIcon onClick={handleClose} icon={faTimes} className="iconTimes"/>
          </div>




          <div className="commentsBody">
            <Comments postid={data.id}/>
          </div>



          <div className="likes">
          { liked 
                    ? <FontAwesomeIcon onClick={like} icon={faHeart} className="iconLiked"></FontAwesomeIcon>
                    : <FontAwesomeIcon onClick={like} icon={faHeart} className="iconLike"></FontAwesomeIcon> }

          <p className="countLikes">{countLike}</p>
          </div>



            <form onSubmit={postComment}>
          <div className="commentsFooter">


              <input ref={commentValue} className="postInput"type="text" placeholder="Add a comment..." required="required"/>
              <button type="submit" className="postButton">Post</button>
          </div>
            </form>


        </div>


        {/* {console.log("DATA - " + data.id)} */}





      </section>
    </div>
  );
};

export default ModalComments