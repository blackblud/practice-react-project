import React, { Component } from 'react'
import Story from './Story'
import { render } from '@testing-library/react'
import { useTranslation } from 'react-i18next'

import storyPic1 from '../img/story/storyPic1.png'
import storyPic2 from '../img/story/storyPic2.png'
import storyPic3 from '../img/story/storyPic3.png'
import storyPic4 from '../img/story/storyPic4.png'
import storyPic5 from '../img/story/storyPic5.png'
import storyPic6 from '../img/story/storyPic6.png'
import storyPic7 from '../img/story/storyPic7.png'
import storyPic8 from '../img/story/storyPic8.png'
import storyPic9 from '../img/story/storyPic9.png'


export default class Stories extends Component {

    

    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            items: [],
            storyPictures: [storyPic1, storyPic2, storyPic3, storyPic4, storyPic5, storyPic6, storyPic7, storyPic8, storyPic9]
        }
    }

    componentDidMount(){
        fetch("https://linkstagram-api.ga/profiles")
        .then(response => response.json())
        .then( 
            (response) => {
                this.setState({
                    isLoaded: true,
                    items: response
                });
            },
            (error) => {
                this.setState({
                    isLoaded: true,
                    error
                })
            }
        )
        
    }

    render(){
       const {error, isLoaded, items} = this.state;
       if(error){
            return <p>Error {error.message}</p>
       } else if (!isLoaded){
            return <p>Story Loading...</p>
       } else {
           return(
            <div className="storyBlock">
                {items.map(item => (
                    item.profile_photo_url==null ? <Story imgscr={this.state.storyPictures[Math.floor(Math.random() * this.state.storyPictures.length)]}/> : <Story imgscr={item.profile_photo_url}/> 
                    )   
                )}
            </div>
           )
       }

    }

}
