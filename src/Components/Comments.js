import React, { Component } from 'react'
import Story from './Story'
import { render } from '@testing-library/react'
import { useTranslation } from 'react-i18next'

import SingleComment from './SingleComment'


export default class Comments extends Component {

    

    constructor(props) {
        super(props);
        this.state = {
            error: null,    
            isLoaded: false,
            comments: []
        }
    }

    componentDidMount(){
        fetch("https://linkstagram-api.ga/posts/" + this.props.postid + "/comments")
        .then(response => response.json())
        .then( 
            (response) => {
                this.setState({
                    isLoaded: true,
                    comments: response
                });
            },
            (error) => {
                this.setState({
                    isLoaded: true,
                    error
                })
            }
        )
        
    }

    render(){
       const {error, isLoaded, comments} = this.state;
       if(error){
            return <p>Error {error.message}</p>
       } else if (!isLoaded){
            return <p>Story Loading...</p>
       } else {
           return(
            <div className="Comments">
                {/* {console.log(this.props.postid + " Output ==== " + comments.id)}
                {console.log("FETCH - " + "https://linkstagram-api.ga/posts/" + this.props.postid + "/comments")} */}

                {/* {comments.map(comment => (
                    console.log("aaaaaaData - " + comment.message)
                    )   
                )} */}



                {comments.map(comment => (
                    <SingleComment data={comment}/> 
                    )   
                )}
            </div>
           )
       }

    }

}
