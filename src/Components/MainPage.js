import React from 'react'
import MainHeader from './MainHeader'
import Stories from './Stories'
import Posts from './Posts'
import UserInfo from './UserInfo'
import { useTranslation } from 'react-i18next'

export default function MainPage() {

    const {t, i18n} = useTranslation();

    function changeLanguage(lang) {
        i18n.changeLanguage(lang);
    }

    return (
        <>
            <MainHeader funct={changeLanguage}/>
            <div className="wrapper">
                <div className="mainBlock">
                    <Stories/>
                    <Posts />
                </div>

                <div className="userInfoBlock">
                    <UserInfo />
                </div>
            </div>
        </>
    )
}
