import React, { Component } from 'react'
import profilePicLink from '../img/profilePicLinkEdit.png'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPlus } from '@fortawesome/free-solid-svg-icons'
import { NavLink } from 'react-router-dom'
import { withTranslation } from 'react-i18next';
import Translation from './Translation'
import ModalEdit from './ModalEdit'
import ModalNew from './ModalNew'
import ReactDOM from 'react-dom' 
import MainHeader from './MainHeader'
import ProfileAllPosts from './ProfileAllPosts'

export default class UserInfo extends Component {

    constructor(props) {
        super();
        this.state = {
            error: null,
            isLoaded: false,
            profileData: [],
            showEdit: false,
            showNew: false
        };
        this.modelEditShow = this.modelEditShow.bind(this);
        this.modelEditHide = this.modelEditHide.bind(this);

        this.modelNewShow = this.modelNewShow.bind(this);
        this.modelNewHide = this.modelNewHide.bind(this);
    }

    modelEditShow() {
        this.setState({ showEdit: true });
    }

    modelEditHide() {
        this.setState({ showEdit: false });
    }

    modelNewShow() {
        this.setState({ showNew: true });
    }

    modelNewHide() {
        this.setState({ showNew: false });
    }


    componentDidMount(){
        fetch("https://linkstagram-api.ga/account", {
            method: 'GET',
            headers: { 
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': localStorage.getItem("Authorization")
            }})
        .then(response => response.json())
        .then(
            (response) => {
                this.setState({
                    isLoaded: true,
                    profileData: response
                });
            },
            (error) => {
                this.setState({
                    isLoaded: true,
                    error
                })
            }
        );
    
    }

    render(){
       const {error, isLoaded, profileData} = this.state;
       if(error){
            return <p>Error {error.message}</p>
       } else if (!isLoaded){
            return <p>Profile Loading...</p>
       } else {
           return(
            <>
                <ModalEdit show={this.state.showEdit} data={profileData} handleClose={this.modelEditHide}/>
                <ModalNew show={this.state.showNew} handleClose={this.modelNewHide}/>

                <MainHeader/>
                <div className="wrapperProfile">
                    <div className="profile">
                        <div className="profileMainInfo">
                            <div className="profileImage">
                                <div className="profileImageInside">
                                    <img className="profileImageInsideIMG" src={profilePicLink}/>
                                </div>
                            </div>
                            <div className="profileData">
                                <p className="pdName"> {profileData.first_name==null || profileData.last_name==null ? 
                                    <p>Name</p> :
                                    <p>{profileData.first_name + ' ' + profileData.last_name}</p>
                                }</p>
                                <p className="pdJob">{
                                
                                profileData.job_title==null ? 
                                    <p>Job Title</p> :
                                    <p>{profileData.job_title}</p>


                                }</p>
                                <p className="pdDesc">{profileData.description==null ? 
                                    <p>Description</p> :
                                    <p>{profileData.description}</p>
                                }</p>
                            </div>
                        </div>




                        <div className="profileAddInfo">
                            <div className="aiFollow">
                                <div className="leftFollow">
                                    <p className="followNumber1">{profileData.followers}</p>
                                    <p className="followText1"><Translation value="followers"/></p>
                                </div>
                                <div className="rightFollow2">
                                    <p className="followNumber2">{profileData.following}</p>
                                    <p className="followText2"><Translation value="following"/></p>
                                </div>
                            </div>
                            <div className="btnInfoo">
                                <button className="btnInfoEditProfilee" onClick={this.modelEditShow}><Translation value="editPorfileBtn"/></button>
                                <button className="btnInfoNewPoste" onClick={this.modelNewShow}><Translation value="newPostBtn"/></button>
                            </div>
                        </div>
                    </div>
                    <div className="profileMobile">
                    <div className="userInfo">
                    <div className="dataInfo">
                        <div className="followersInfo"> 
                            <p>{profileData.followers}</p>
                            <Translation value="followers"/>
                        </div>
                        <div className="profilePicInfo">
                            <div className="profilePicInfoInside">
                                <img className="profilePicInfoPicture" src={profileData.profile_photo_url} />
                                <button className="btnInfoNewPostMin">
                                    <FontAwesomeIcon icon={faPlus} className="iconPlus"/>
                                </button>
                            </div>
                        </div>
                        <div className="followingInfo">
                            <p>{profileData.following}</p>
                            <Translation value="following"/>
                        </div>
                    </div>

                    <div className="nameInfo">
                    {profileData.first_name==null || profileData.last_name==null ? 
                        <p>Name</p> :
                        <p>{profileData.first_name + ' ' + profileData.last_name}</p>
                    }
                        
                        <span>-</span>
                        {profileData.job_title==null ? 
                        <p>Job Title</p> :
                        <p>{profileData.job_title}</p>
                    }
                    </div>


                    <div className="bioInfo">
                        {profileData.description==null ? 
                            <p>Description</p> :
                            <p>{profileData.description}</p>
                        }
                    </div>


                    <div className="btnInfo">
                        <button className="btnInfoEditProfile" onClick={this.modelEditShow}><Translation value="editPorfileBtn"/></button>
                        <button className="btnInfoNewPost" onClick={this.modelNewShow}><Translation value="newPostBtn"/></button>
                    </div>
                </div>
                    </div>


                    
                    <ProfileAllPosts userData={profileData}/>

                </div>

                {/* <ModalEdit show={this.state.showEdit} data={profileData} handleClose={this.modelEditHide}/>
                <ModalNew show={this.state.showNew} handleClose={this.modelNewHide}/>


                <div className="btnInfo">
                    <button className="btnInfoEditProfile" onClick={this.modelEditShow}><Translation value="editPorfileBtn"/></button>
                    <button className="btnInfoNewPost" onClick={this.modelNewShow}><Translation value="newPostBtn"/></button>
                </div> */}
            
            </>
           )
       }

    }
}