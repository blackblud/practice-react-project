import React from 'react'
import { useTranslation } from 'react-i18next'


export default function Translation(props) {
    const {t, i18n} = useTranslation();
    return (
            <p>{t(props.value)}</p>
    )
}
