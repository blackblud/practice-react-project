import React from 'react'
import testPhoto from '../img/story/storyPic4.png'

export default function Story(props) {
    return(
        <div className="story">
            <div className="storyImage">
            <img className="storyImageImg" src={props.imgscr} />
            </div>
        </div>
    )
}
