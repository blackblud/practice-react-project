import React, { Component } from 'react'
import Post from './Post'
import Story from './Story'
import { render } from '@testing-library/react'
import { useTranslation } from 'react-i18next'

import storyPic1 from '../img/story/storyPic1.png'
import storyPic2 from '../img/story/storyPic2.png'
import storyPic3 from '../img/story/storyPic3.png'
import storyPic4 from '../img/story/storyPic4.png'
import storyPic5 from '../img/story/storyPic5.png'
import storyPic6 from '../img/story/storyPic6.png'
import storyPic7 from '../img/story/storyPic7.png'
import storyPic8 from '../img/story/storyPic8.png'
import storyPic9 from '../img/story/storyPic9.png'


export default class Posts extends Component {

    

    constructor(props) {
        super(props);
        this.state = {
            error: null,    
            isLoaded: false,
            posts: [],
            reservePictures: [storyPic1, storyPic2, storyPic3, storyPic4, storyPic5, storyPic6, storyPic7, storyPic8, storyPic9]
        }
    }

    componentDidMount(){

        let dataRequest;

        if(localStorage.getItem("Authorization")){
            console.log("Auth");
            dataRequest = {
                method: 'GET',
                headers: { 
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': localStorage.getItem("Authorization")
                }
            };
        } else {
            dataRequest = {
                method: 'GET',
                headers: { 
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            };
        }

        fetch("https://linkstagram-api.ga/posts", dataRequest)
        .then(response => response.json())
        .then( 
            (response) => {
                this.setState({
                    isLoaded: true,
                    posts: response
                });
            },
            (error) => {
                this.setState({
                    isLoaded: true,
                    error
                })
            }
        );
        // const {reservePictures, posts} = this.state;
        // posts.map(post => {
        //     post.randomImage = Math.floor(Math.random() * reservePictures.length);
        // })
        
        // console.log("TTT - " + reservePictures);
        
        // я тут хотів передати в масив об'єктів ще одне поле "randomImage", щоб не генерувати числа кожного рендеру
    }
    
    render(){
        const {error, isLoaded, posts, reservePictures} = this.state;
       if(error){
            return <p>Error {error.message}</p>
       } else if (!isLoaded){
            return <p>Posts Loading...</p>
       } else {
           return(
            <div className="postsBlock">
                {posts.map(post => 
                    <Post data={post}/>
                    )   
                }
            </div>
           )
       }

    }

}
