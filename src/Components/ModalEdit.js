import React, { createRef, useState } from 'react';
import { useHistory } from 'react-router-dom'
import profilePicLinkEdit from '../img/profilePicLinkEdit.png'
import Translation from './Translation'

const ModalEdit = ({ handleClose, show, data }) => {
  const showHideClassName = show ? "modal display-block" : "modal display-none";

  let firstName = createRef();
  let lastName = createRef();
  let jobTitle = createRef();
  let description = createRef();

  const history = useHistory();

  const [valueFirstName, setValueFirstName] = useState(data.first_name);
  const [valueLastName, setValueLastName] = useState(data.last_name);
  const [valueJobTitle, setValuejobTitle] = useState(data.job_title);
  const [valueDescription, setValueDescription] = useState(data.description);
  function handerChangeFN(e){
    setValueFirstName(e.target.value);
  }
  function handerChangeLN(e){
    setValueLastName(e.target.value);
  }
  function handerChangeJT(e){
    setValuejobTitle(e.target.value);
  }
  function handerChangeDS(e){
    setValueDescription(e.target.value);
  }

  function clearInputs(){
    setValueFirstName(data.first_name);
    setValueLastName(data.last_name);
    setValuejobTitle(data.job_title);
    setValueDescription(data.description);
  }

  function logOut(){
    localStorage.removeItem("Authorization");
    history.push("/signin");
  }

  function updateUserInfo(e){
    e.preventDefault();

    // console.log("firstName - " + firstName.current.value);
    // console.log("lastName - " + lastName.current.value);
    // console.log("jobTitle - " + jobTitle.current.value);
    // console.log("description - " + description.current.value);

    const dataRequest = {
      method: 'PATCH',
      headers: { 
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': localStorage.getItem("Authorization")
      },
      body: JSON.stringify({
          "account": {
            "username": data.username,
            "profile_photo": {
              "id": "85ec3b3cae2c5e6972ac9e1c3d29aa5a",
              "storage": "cache",
              "metadata": {
                "size": 68393,
                "mime_type": "image/jpeg",
                "filename": "test.jpg"
              }
            },
            "description": description.current.value,
            "first_name": firstName.current.value,
            "last_name": lastName.current.value,
            "job_title": jobTitle.current.value
          }
      })
    };

    fetch('https://linkstagram-api.ga/account', dataRequest)
    .then(response => {
        if(response.ok){
          console.log("Final - all good!");
          window.location.reload();
        } else {
          console.log("Smth bad...");
        }
    })
    }

  function uploadPhoto(e){
      console.log("Lol");
  }

  return (
    <div className={showHideClassName}>
      <section className="modal-main">

        <div className="editHeader">
          <p className="editHeaderProfileInfoText"><Translation value="profileInfo"/></p>
          <button onClick={logOut}><Translation value="logOut"/></button>
        </div>

        <form onSubmit={updateUserInfo}>

          <div className="editBody">
            <div className="editBodyUp">
              <div className="editBodyUpImage">
                <img className="editProfileImage" src={profilePicLinkEdit}/>



                <label for="inpPh" class="chooseNewPicBtn">
                  <div className="chooseNewPicBtn" onClick={uploadPhoto}><p><Translation value="choosePhoto"/></p></div>
                  <p><Translation value="choosePhoto"/></p>
                  <input id="inpPh" type="file"/>
                </label>





              </div>
              <div className="editBodyUpIput">

              <div className="inputDivEdit"> 
                  <p className="inputNameEdit"><Translation value="firstName"/></p>
                  <input ref={firstName} value={valueFirstName} onChange={handerChangeFN} className="inputTypeEdit" type="text" required="required"/>
              </div>
              <div className="inputDivEdit"> 
                  <p className="inputNameEdit"><Translation value="secondName"/></p>
                  <input ref={lastName} value={valueLastName} onChange={handerChangeLN} className="inputTypeEdit" type="text" required="required"/>
              </div>


              </div>
            </div>



            <div className="editBodyDownEdit">
            <div className="inputDivEdit"> 
                  <p className="inputNameEdit"><Translation value="jobTitle"/></p>
                  <input ref={jobTitle} value={valueJobTitle} onChange={handerChangeJT} className="inputTypeEdit" type="text" required="required"/>
              </div>

              <div className="inputDivEdit"> 
                  <p className="textareaNameEdit"><Translation value="desc"/></p>
                  <textarea ref={description} value={valueDescription} onChange={handerChangeDS} className="textareaTypeEdit" type="textarea" required="required"/>
              </div>
            </div>
          </div>



        <div className="editFooter">
          <button className="modalBtnCancel" type="button" onClick={() => {handleClose(); clearInputs();}}><Translation value="cancel"/></button>
          <button type="submit" className="modalBtnSave"><Translation value="save"/></button>
        </div>
      
        </form>
      </section>
    </div>
  );
};

export default ModalEdit