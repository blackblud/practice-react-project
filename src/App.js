import React from 'react'
import SignUpPage from './Components/SignPages/SignUpPage'
import SignInPage from './Components/SignPages/SignInPage'
import MainPage from './Components/MainPage'
import None404 from './Components/None404'
import Profile from './Components/Profile'
import { useTranslation } from 'react-i18next'

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

function App() {

  return (
    <div className="App">
      <Router>

          <Switch>
            <Route exact path="/" component={MainPage}></Route>
            <Route exact path="/signup" component={SignUpPage}></Route>
            <Route exact path="/signin" component={SignInPage}></Route>
            <Route exact path="/profile" component={Profile}></Route>
            <Route component={None404}></Route>
          </Switch>

      </Router>
    </div>
  );
}

export default App;
